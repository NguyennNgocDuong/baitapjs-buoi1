// BÀI 1
/**
 * input: số ngày làm
 * step: 
 *      s1: tạo biến chứa số ngày làm
 *      s2: lương 1 ngày nhân với biến chứa số ngày làm
 * output: lương
 */
var workDay = 5;
var salaryOfDay = 100000;
var salary = workDay * salaryOfDay
console.log(salary);

// BÀI 2
/**
 * input: 5 số thực 
 * step: 
 *      s1: gán 5 số thực cho 5 biến
 *      s1:tổng 5 biến và chia cho 5
 *  output: giá trị trung bình của 5 số thực
 */

var a = 5;
var b = 10;
var c = 15;
var d = 20;
var e = 25;

var avg = (a + b + c + d + e) / 5
console.log(avg);

// BÀI 3
/**
 * input: số tiền usd
 * step:
 *      s1: gán số tiền usd vào 1 biến
 *      s2:biến được gán nhân cho 23.500
 * output:số tiền vnd
 */

var usd = 2;

var vnd = usd * 23500
console.log(vnd);

// BÀI 4
/**
 * input: chiều dài,chiều rộng
 * step:
 *      s1: gán chiều dài, chiều rộng vào 2 biến
 *      s2: 2 biến chiều dài và chiều rộng nhân nhau ra được diện tích
 *      s3  tổng 2 biến chiều dài và chiều rộng chia 2 ra được chu vi
 * output: chu vi và diện tích hình chữ nhật
 */

var chieuDai = 4;
var chieuRong = 2;

var dienTich = chieuDai * chieuRong;
console.log('dienTich: ', dienTich);
var chuVi = (chieuDai + chieuRong) / 2
console.log('chuVi: ', chuVi);

// BÀI 5
/**input: số có 2 chữu số
 * step: 
 *      s1:gán số có 2 chữ số vào 1 biến
 *      s2:lấy số đã gán % 10 để lấy số ở hàng đơn vị
 *      s3: lấy số đã gán / 10 và làm tròn để lấy số ở hàng chục
 * output: tổng các ký số
 * 
 */

var so = 76;

var soHangDv = so % 10;
var soHangChuc = Math.floor(so / 10);

var tongKySo = soHangDv + soHangChuc
console.log('tongKySo: ', tongKySo);

